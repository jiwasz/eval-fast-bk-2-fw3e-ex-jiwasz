﻿# FORT Technical Exercise Explanation

## Summary

The original Machine class had several usability and maintainablity issues included, but not limited to:

- machineName and type fields should be set using a constructor. Chaning the fields after creation can lead to inconsistent behavior (e.g. machine colors can change after instantiation if the machine type changes). 
- Properties accessors use hardcoded values to compare type values
- Colors are expressed as hardcoded string values and repeated (e.g. "red" appears on lines 64 and 83)

Each property uses the type to drive which values are returned in property accessors. This establishes an association between the machine type (i.e. 0, 1, 2, etc.) and corresponding attributes (i.e. Color, MaxSpeed, Trim, Name, etc). A manifest file clearly associates the 
type with attributes. The following general steps were take to refactor in effort to improve maintainability, usabilty, and not expose unnecessary public methods.

Refactoring was applied to achieve the following goals:

- improve maintainability
- improve usability
- expose minimal public classes, methods, and properties


## Defined Enumerations 
Enumations define the machine type and list of colors meeting two goals:

- Central maintenance of enumerated names and values
- Limits the set of available choices

## Re-evaluated Public Methods

The _IsDark_method was refactored out of the Machine class and placed in a static MachineUtility class. Unless there is a need for external callers to resolve the 
IsDark boolean value for a _MachineColor_, then this should be internal. 

Logic applied in the _GetMaxSpeed_ method was refactored into the _MaxSpeed_ property and simplified.

## Redefined Machine class

The _Machine_ class has limited logic and now serves largely as a [POCO](https://stackoverflow.com/questions/250001/poco-definition) with minimal business logic to define the description and maximum speed.

An internal constructor prevents consumers from creating the object directly. Instead, a dictionary creates a manifest which uses the internal constructor to make the same
type-to-attribute values that were previously hardcoded (e.g. Bulldozer color is Red). 

```
{ (int) MachineType.Bulldozer, new Machine(MachineType.Bulldozer, MachineColor.Red, true, 80, null) },
{ (int) MachineType.Crane, new Machine(MachineType.Crane, MachineColor.Blue, true, 75, MachineColor.Blue.IsDark() ? MachineColor.Black : MachineColor.White) },
{ (int) MachineType.Tractor, new Machine(MachineType.Tractor,  MachineColor.Green, true, 90, MachineColor.Green.IsDark() ?  MachineColor.Gold : null) },
{ (int) MachineType.Truck, new Machine(MachineType.Truck, MachineColor.Yellow, false, DEFAULT_MAX_SPEED, MachineColor.Yellow.IsDark() ? MachineColor.Silver : null) },
{ (int) MachineType.Car, new Machine(MachineType.Car,  MachineColor.Brown, false, 90, null) },
{ 5, new Machine(5) }
```

Please note that the dictionary includes a member that doesn't correspond to a known _MachineType_ which applied default _Machine_ class settings.

## Added MachineRepository

Consumers are provided with a new _IMachineRepository_ with a single method to retrieve a machine by type.

```
public Machine GetMachine(MachineType machineType);
```

The _MachineRepository_ implementation retrieves the association from the static manifest dictionary. This sets the stage to refactor the manifest into a database. Another
_IMachineRepository_ implementation could maintain the same contract and retrieve the machine list from a database instead.

## Added Test Project

An integration test in the new FortCodeExercises.Tests validates that the expected machine configurations are returned.


## Updated to NET Core 5

If there are no hard requirements on the Core or Framework versions, then my preference is to start with the latest available. If this applies to a production project, then I would prefer the latest version with LTS.

FortCodeExcercises project was upgraded from .NET Core 3.1 to .NET 5.0.