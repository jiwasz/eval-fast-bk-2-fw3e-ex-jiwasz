﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public interface IMachineRepository
    {
        public Machine GetMachine(int type);

    }
}
