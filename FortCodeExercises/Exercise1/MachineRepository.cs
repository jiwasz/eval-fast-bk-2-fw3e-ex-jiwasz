﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class MachineRepository : IMachineRepository
    {
        public Machine GetMachine(int type)
        {
            return MachineUtility.GetMachineDefinition(type);
        }
    }
}
