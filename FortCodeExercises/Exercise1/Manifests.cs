﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    internal static class Manifests
    {
        internal const int DEFAULT_MAX_SPEED = 70;

        internal static Dictionary<MachineColor, bool> MachineColorManifest = new Dictionary<MachineColor, bool>()
        {
            { MachineColor.BabyBlue, false },
            { MachineColor.Beige, false },
            { MachineColor.Black, true },
            { MachineColor.Blue, true },
            { MachineColor.Brown, false },
            { MachineColor.Crimson, true },
            { MachineColor.Gold, false },
            { MachineColor.Green, true },
            { MachineColor.Red, true },
            { MachineColor.Silver, false },
            { MachineColor.White, false },
            { MachineColor.Yellow, false }
        };


        internal static Dictionary<int, Machine> MachineManifest = new Dictionary<int, Machine>()
        {
            // if (machineType == 1 && noMax == false) max = 70;
            // An explicit rule previsouly sets the normal maximum speed for a bulldozer to 70 if noMax is applied in GetMaxSpeed. 
            // If the default maximum speed changes, then this may not reflect the intention of this explicit rule 
            // (e.g. override the default max speed set by DEFAULT_MAX_SPEED with 70 for the bulldozer so it applies even if the default changes for all other vehicles)
            // TODO: A subject matter expert should be consulted to validate the intention.
            { (int) MachineType.Bulldozer, new Machine(MachineType.Bulldozer, MachineColor.Red, true, 80, null) },
            { (int) MachineType.Crane, new Machine(MachineType.Crane, MachineColor.Blue, true, 75, MachineColor.Blue.IsDark() ? MachineColor.Black : MachineColor.White) },
            { (int) MachineType.Tractor, new Machine(MachineType.Tractor,  MachineColor.Green, true, 90, MachineColor.Green.IsDark() ?  MachineColor.Gold : null) },
            { (int) MachineType.Truck, new Machine(MachineType.Truck, MachineColor.Yellow, false, DEFAULT_MAX_SPEED, MachineColor.Yellow.IsDark() ? MachineColor.Silver : null) },
            { (int) MachineType.Car, new Machine(MachineType.Car,  MachineColor.Brown, false, 90, null) },
            { 5, new Machine(5) }
        };
    }
}
