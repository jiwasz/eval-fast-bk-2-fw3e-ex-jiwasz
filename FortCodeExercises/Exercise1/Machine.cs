﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Machine
    {

        internal Machine(int machineType)
        {
            Type = machineType;
            Color = MachineColor.White;

            UseMachineSpeed = true;

            MachineMaxSpeed = Manifests.DEFAULT_MAX_SPEED;       
        }

        internal Machine(MachineType type, MachineColor color, bool useMachineSpeed, int maxSpeed, MachineColor? trim)
        {
            Type = (int) type;

            Name = type.ToString();

            Color = color;

            MachineMaxSpeed = maxSpeed;

            UseMachineSpeed = useMachineSpeed;

            Trim = trim; 
        }

        public string Name { get;  }

        public int Type { get; }
      
        public MachineColor Color { get; }

        /// <summary>
        /// This is the publicaly available maximum speed.
        /// </summary>
        public int MaxSpeed
        {
            get
            {
                return this.UseMachineSpeed ? this.MachineMaxSpeed : Manifests.DEFAULT_MAX_SPEED;
            }
        }

        public MachineColor? Trim { get; }

        public string Description {
            get
            {                
                return $" {this.Color} {this.Name} [{this.MaxSpeed}].";
            }
        }

        /// <summary>
        /// The vehicle's actual maximum speed.
        /// </summary>
        internal int MachineMaxSpeed { get; }

        internal bool UseMachineSpeed { get;  }
    }
}
