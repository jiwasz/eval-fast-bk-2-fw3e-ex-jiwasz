﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public enum MachineColor
    {
        White,
        Blue,
        Red,
        Brown,
        Yellow,
        Green,
        Beige,
        Black,
        BabyBlue,
        Crimson,
        Gold,
        Silver
    }
}
