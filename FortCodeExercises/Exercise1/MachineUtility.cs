﻿using System;

namespace FortCodeExercises.Exercise1
{
    internal static class MachineUtility
    {


        internal static bool IsDark(this MachineColor color)
        {
            if (Manifests.MachineColorManifest.ContainsKey(color))
            {
                return Manifests.MachineColorManifest[color];
            }
            else
            {
                return false;
                // Could optionally throw an exception if business rules require isDark to be set per MachineColor
                // throw new Exception($"IsDark {color} mapping not found");
            }
        }

        internal static Machine GetMachineDefinition(int machineType)
        {
            if (Manifests.MachineManifest.ContainsKey(machineType))
            {
                return Manifests.MachineManifest[machineType];
            }
            else
            {
                throw new Exception($"Machine type {machineType} not found");
            }
        }

    }
}
