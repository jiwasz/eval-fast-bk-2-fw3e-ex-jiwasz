using System;
using Xunit;
using FortCodeExercises.Exercise1;
using System.Diagnostics;

namespace FortCodeExercises.Tests
{
    public class MachineTests
    {

        [Theory]
        [InlineData((int) MachineType.Bulldozer, MachineColor.Red, 80, null, " Red Bulldozer [80].")]
        [InlineData((int) MachineType.Crane, MachineColor.Blue, 75, MachineColor.Black, " Blue Crane [75].")]
        [InlineData((int) MachineType.Tractor, MachineColor.Green, 90, MachineColor.Gold, " Green Tractor [90].")]
        [InlineData((int) MachineType.Truck, MachineColor.Yellow, 70, null, " Yellow Truck [70].")]
        [InlineData((int) MachineType.Car, MachineColor.Brown, 70, null, " Brown Car [70].")]
        [InlineData(5, MachineColor.White, 70, null, " White  [70].")]
        public void MachineNameTest(int type, MachineColor expectedColor, int maxSpeed, MachineColor? expectedTrim, string expectedDescription)
        {

            IMachineRepository machineRep = new MachineRepository();

            Machine foundMachine = machineRep.GetMachine(type);

            Assert.NotNull(foundMachine);

            Assert.Equal(expectedColor, foundMachine.Color);

            Assert.Equal(maxSpeed, foundMachine.MaxSpeed);

            Assert.Equal(expectedTrim, foundMachine.Trim);

            Assert.Equal(expectedDescription, foundMachine.Description);
        }
    }
}
